package cn.yunhe.action.temp;

import cn.yunhe.pojo.Book;
import cn.yunhe.service.BookService;
import cn.yunhe.util.Pagination;
import com.opensymphony.xwork2.ActionSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookAction3 extends ActionSupport {

    private Map<String,Object>map=new HashMap<>();
    private int bid;



    BookService bookService=new BookService();
    @Override
    public String execute() {

        Book book=new Book();
        System.out.println("<bbbb====>"+bid);
        book.setBid(bid);
        book.setBisbn("11111");
        book.setBname("ddddd");
        map.put("book",book);

        List<Book> bookList=bookService.queryByBname("");
        Pagination<Book> pagination=new Pagination<>();
        pagination.setList(bookList);
        pagination.setTotalRecords(100);
        pagination.setMaxRows(15);
        pagination.setCurrPageNo(5);
        map.put("pagination",pagination);

        return "success";
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }
}
