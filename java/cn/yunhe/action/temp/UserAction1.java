package cn.yunhe.action.temp;

import cn.yunhe.util.BaseAction;

import java.io.PrintWriter;

public class UserAction1 extends BaseAction {
    private String uname;
    private String upwd;

    @Override
    public String execute() throws Exception {
        /*PrintWriter out=getResponse().getWriter();
        out.print("<==帐号===>"+uname+"<==密码===>"+upwd);*/
        getMap2Request().put("uname1",uname);
        getMap2Request().put("upwd1",upwd);
        return super.execute();
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUpwd() {
        return upwd;
    }

    public void setUpwd(String upwd) {
        this.upwd = upwd;
    }
}
