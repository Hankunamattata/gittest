package cn.yunhe.action.temp;

import cn.yunhe.pojo.Book;
import cn.yunhe.service.BookService;
import cn.yunhe.util.Pagination;
import com.opensymphony.xwork2.ActionSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookAction2 extends ActionSupport {
    private Pagination<Book> pagination;
    private Book book;

    private Map<String,Object>map=new HashMap<>();


    BookService bookService=new BookService();
    @Override
    public String execute() {

        book=new Book();
        book.setBid(111);
        book.setBisbn("111111111");
        book.setBname("xxxxx");
        map.put("book",book);

        List<Book> bookList=bookService.queryByBname("");
        pagination=new Pagination<>();
        pagination.setList(bookList);
        map.put("pagination",pagination);



        return "success";
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }
}
