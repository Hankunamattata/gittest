package cn.yunhe.action.temp;

import cn.yunhe.pojo.Book;
import cn.yunhe.service.BookService;
import cn.yunhe.util.Pagination;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookAction4 extends ActionSupport implements ModelDriven {

    private Map<String,Object>map=new HashMap<>();
    private Book book;



    BookService bookService=new BookService();
    @Override
    public String execute() {

        Book book1=new Book();
        System.out.println("<bbbb====>"+book.getBid());
        book1.setBid(book.getBid());
        book1.setBisbn("11111");
        book1.setBname("ddddd");
        map.put("book",book1);

        List<Book> bookList=bookService.queryByBname("");
        Pagination<Book> pagination=new Pagination<>();
        pagination.setList(bookList);
        pagination.setTotalRecords(100);
        pagination.setMaxRows(15);
        pagination.setCurrPageNo(5);
        map.put("pagination",pagination);

        return "success";
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public Object getModel() {
        if (book==null){
            book=new Book();
        }
        return book;
    }
}
