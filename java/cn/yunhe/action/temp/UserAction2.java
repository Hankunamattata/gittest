package cn.yunhe.action.temp;

import cn.yunhe.pojo.User;
import cn.yunhe.util.BaseAction;

public class UserAction2 extends BaseAction {
    private User user;

    @Override
    public String execute() throws Exception {
        getMap2Request().put("user1",user);
        return super.execute();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
