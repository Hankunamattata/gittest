package cn.yunhe.action.temp;

import cn.yunhe.pojo.Book;
import cn.yunhe.service.BookService;
import cn.yunhe.util.Pagination;
import com.opensymphony.xwork2.ActionSupport;

import java.util.List;

public class BookAction1 extends ActionSupport {
    private Pagination<Book> pagination;
    private Book book;

    BookService bookService=new BookService();
    @Override
    public String execute() {



        List<Book> bookList=bookService.queryByBname("");
        pagination=new Pagination<>();
        pagination.setList(bookList);
        System.out.println(pagination.getList().size());

        book=new Book();
        book.setBid(111);
        book.setBisbn("111111111");
        book.setBname("wwww");

        return "success";
    }


    public Pagination<Book> getPagination() {
        return pagination;
    }

    public void setPagination(Pagination<Book> pagination) {
        this.pagination = pagination;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
