package cn.yunhe.action.temp;

import cn.yunhe.pojo.User;
import cn.yunhe.util.BaseAction;
import com.opensymphony.xwork2.ModelDriven;

public class UserAction3 extends BaseAction implements ModelDriven {
    private User user;

    @Override
    public String execute() throws Exception {
        getMap2Request().put("user1",user);
        return super.execute();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public User getModel() {
        if (user==null){
            user=new User();
        }
        return user;
    }
}
