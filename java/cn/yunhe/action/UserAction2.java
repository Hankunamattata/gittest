package cn.yunhe.action;

import cn.yunhe.pojo.User;
import cn.yunhe.util.BaseAction;
import com.opensymphony.xwork2.ModelDriven;

import java.io.PrintWriter;

public class UserAction extends BaseAction implements ModelDriven {
    private User user;
    public String name111;
    private String age222;

    @Override
    public String execute() throws Exception {
        getMap2Request().put("user2",user);
        return super.execute();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public User getModel() {
        if (user==null){
            user=new User();
        }
        return user;
    }
}
