package cn.yunhe.action;

import cn.yunhe.pojo.Book;
import cn.yunhe.service.BookService;
import cn.yunhe.util.Pagination;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookAction  extends ActionSupport{

    private Map<String,Object>map=new HashMap<>();
    private int bid;

    BookService bookService=new BookService();
    @Override
    public String execute() {

        Book book1=new Book();
        System.out.println("<bbbb====>"+bid);
        book1.setBid(bid);
        book1.setBisbn("11111");
        book1.setBname("ddddd");
        map.put("book",book1);

        List<Book> bookList=bookService.queryByBname("");
        Pagination<Book> pagination=new Pagination<>();
        pagination.setList(bookList);
        pagination.setTotalRecords(100);
        pagination.setMaxRows(15);
        pagination.setCurrPageNo(5);
        map.put("pagination",pagination);

        return "success";
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }
}
