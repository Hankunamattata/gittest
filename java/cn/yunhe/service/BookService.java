package cn.yunhe.service;

import cn.yunhe.dao.BookDao;
import cn.yunhe.pojo.Book;
import cn.yunhe.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class BookService {
    SqlSession session=null;
    BookDao bookDao=null;
    
    public void initial(){
        session=SqlSessionFactoryUtil.getSqlSession();
        bookDao=session.getMapper(BookDao.class);
    }
    
    public void destory(){
        if (session!=null){
            session.commit();
            session.close();
        }
    }

    public List<Book> queryByBname(String bname){
        initial();
        List<Book> bookList=bookDao.queryByBname(bname);
        destory();
        return bookList;

    }


    public List<Book> queryByBauthor(String bauthor){
        initial();
        List<Book> bookList=bookDao.queryByBauthor(bauthor);
        destory();
        return bookList;
    }


    public List<Book> queryByBpress(String bpress){
        initial();
        List<Book> bookList=bookDao.queryByBpress(bpress);
        destory();
        return bookList;
    }

    public String[] searchBname(String keywords){
        initial();
        String[] bname2Array=bookDao.searchBname(keywords);
        destory();
        return bname2Array;
    }
}
