package cn.yunhe.dao;

import cn.yunhe.pojo.Book;

import java.util.List;

public interface BookDao {
    List<Book> queryByBname(String bname);
    List<Book> queryByBauthor(String bauthor);
    List<Book> queryByBpress(String bpress);

    String[] searchBname(String keywords);

}