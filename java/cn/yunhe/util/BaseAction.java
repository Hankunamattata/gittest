package cn.yunhe.util;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class BaseAction extends ActionSupport {

    public Map<String,Object> getMap2Request(){
        return (Map<String, Object>) ActionContext.getContext().get("request");
    }

    public Map<String,Object> getMap2Session(){
        return ActionContext.getContext().getSession();
    }

    public HttpServletRequest getRequest(){

        //HttpServletRequest request=ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        HttpServletRequest request=ServletActionContext.getRequest();
        return request;
    }

    public HttpSession getSession(){
        return ServletActionContext.getRequest().getSession();
    }

    public HttpServletResponse getResponse(){

        //HttpServletResponse response= (HttpServletResponse) ActionContext.getContext().get(ServletActionContext.HTTP_RESPONSE);
        HttpServletResponse response=ServletActionContext.getResponse();
        response.setContentType("text/html;charset=utf-8");

        return response;
    }
}
