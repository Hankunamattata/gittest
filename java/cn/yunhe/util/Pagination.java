package cn.yunhe.util;

import java.io.Serializable;
import java.util.List;

public class Pagination<T> implements Serializable {
    private int totalRecords;
    private int maxRows;
    private int currPageNo;

    private int totalPages;
    private int prePageNo;
    private int nextPageNo;

    private List<T> list;

    public Pagination() {
    }

    public Pagination(int totalRecords, int maxRows, int currPageNo, int totalPages, int prePageNo, int nextPageNo, List<T> list) {
        this.totalRecords = totalRecords;
        this.maxRows = maxRows;
        this.currPageNo = currPageNo;
        this.totalPages = totalPages;
        this.prePageNo = prePageNo;
        this.nextPageNo = nextPageNo;
        this.list = list;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public int getMaxRows() {
        return maxRows;
    }

    public void setMaxRows(int maxRows) {
        this.maxRows = maxRows;
    }

    public int getCurrPageNo() {
        return currPageNo;
    }

    public void setCurrPageNo(int currPageNo) {
        this.currPageNo = currPageNo;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getPrePageNo() {
        return prePageNo;
    }

    public void setPrePageNo(int prePageNo) {
        this.prePageNo = prePageNo;
    }

    public int getNextPageNo() {
        return nextPageNo;
    }

    public void setNextPageNo(int nextPageNo) {
        this.nextPageNo = nextPageNo;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
